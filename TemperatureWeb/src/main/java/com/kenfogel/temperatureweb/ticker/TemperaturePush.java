/*

 DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.

 Copyright (c) 2015 C2B2 Consulting Limited. All rights reserved.

 The contents of this file are subject to the terms of the Common Development
 and Distribution License("CDDL") (collectively, the "License").  You
 may not use this file except in compliance with the License.  You can
 obtain a copy of the License at
 https://glassfish.dev.java.net/public/CDDL+GPL_1_1.html
 or packager/legal/LICENSE.txt.  See the License for the specific
 language governing permissions and limitations under the License.

 When distributing the software, include this License Header Notice in each
 file and include the License file at packager/legal/LICENSE.txt.
 */
/**
 * This demo is based on the PiyaraMicroDemo by Steve Millidge that can be found
 * at https://github.com/smillidge/PiyaraMicroDemo
 * The GrovePi+ libraries used in this project are based on the work of
 * Eduardo Moranchel that can be found at https://github.com/emoranchel/IoTDevices
 * and a modified version of Eduardo's work is included in this repository
 * The GrovePi+ libraries that depend on Pi4J are maintained at http://pi4j.com/
 * and are retrieved by Maven.
 */
package com.kenfogel.temperatureweb.ticker;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author steve
 */
@ServerEndpoint("/graph")
public class TemperaturePush {

    private static final long serialVersionUID = 1L;

    @Inject
    TemperatureSessionManager sessionManager;

    private Session mySession;

    @OnOpen
    public void onOpen(Session session) {
        Logger.getLogger(TemperaturePush.class.getName()).log(Level.INFO, "Opened Session: " + session.getId());

        mySession = session;
        sessionManager.registerSession(session);
    }

    @OnClose
    public void onClose(Session session) {
        Logger.getLogger(TemperaturePush.class.getName()).log(Level.INFO, "Closed Session: " + session.getId());
        sessionManager.deregisterSession(session);
    }

    @OnMessage
    public String onMessage(String message, Session session) {
        return null;
    }

    @OnError
    public void onError(Throwable t) {
        Logger.getLogger(TemperaturePush.class.getName()).log(Level.SEVERE, "Session error: ", t);
        sessionManager.deregisterSession(mySession);
    }

}
