/*

 DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.

 Copyright (c) 2015 C2B2 Consulting Limited. All rights reserved.

 The contents of this file are subject to the terms of the Common Development
 and Distribution License("CDDL") (collectively, the "License").  You
 may not use this file except in compliance with the License.  You can
 obtain a copy of the License at
 https://glassfish.dev.java.net/public/CDDL+GPL_1_1.html
 or packager/legal/LICENSE.txt.  See the License for the specific
 language governing permissions and limitations under the License.

 When distributing the software, include this License Header Notice in each
 file and include the License file at packager/legal/LICENSE.txt.
 */
/**
 * This demo is based on the PiyaraMicroDemo by Steve Millidge that can be found
 * at https://github.com/smillidge/PiyaraMicroDemo
 * The GrovePi+ libraries used in this project are based on the work of
 * Eduardo Moranchel that can be found at https://github.com/emoranchel/IoTDevices
 * and a modified version of Eduardo's work is included in this repository
 * The GrovePi+ libraries depend on Pi4J are maintained at http://pi4j.com/
 * and are retrieved by Maven.
 */
package com.kenfogel.temperatureweb.ticker;

import com.kenfogel.temperature.bean.Temperature;
import fish.payara.micro.cdi.ClusteredCDIEventBus;
import fish.payara.micro.cdi.Inbound;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.websocket.Session;

/**
 *
 * @author steve
 */
@ApplicationScoped
public class TemperatureSessionManager {

    private HashSet<Session> sessions;

    @Inject
    private ClusteredCDIEventBus bus;

    @PostConstruct
    public void postConstruct() {
        bus.initialize();
        sessions = new HashSet<>();
    }

    void registerSession(Session session) {
        sessions.add(session);
    }

    void deregisterSession(Session session) {
        sessions.remove(session);
    }

    public void observer(@Observes @Inbound Temperature temperatureBean) {
        try {
            for (Session session : sessions) {
                Logger.getLogger(TemperaturePush.class.getName()).log(Level.INFO, "Recieved " + temperatureBean.toString() + " writing to " + session.getId());
                session.getBasicRemote().sendText(temperatureBean.toString());
            }
        } catch (IOException ex) {
            Logger.getLogger(TemperaturePush.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
