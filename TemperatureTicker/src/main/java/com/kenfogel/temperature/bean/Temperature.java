/*

 DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.

 Copyright (c) 2015 C2B2 Consulting Limited. All rights reserved.

 The contents of this file are subject to the terms of the Common Development
 and Distribution License("CDDL") (collectively, the "License").  You
 may not use this file except in compliance with the License.  You can
 obtain a copy of the License at
 https://glassfish.dev.java.net/public/CDDL+GPL_1_1.html
 or packager/legal/LICENSE.txt.  See the License for the specific
 language governing permissions and limitations under the License.

 When distributing the software, include this License Header Notice in each
 file and include the License file at packager/legal/LICENSE.txt.
 */
/**
 * This demo is based on the PiyaraMicroDemo by Steve Millidge that can be found
 * at https://github.com/smillidge/PiyaraMicroDemo
 * The GrovePi+ libraries used in this project are based on the work of
 * Eduardo Moranchel that can be found at https://github.com/emoranchel/IoTDevices
 * and a modified version of Eduardo's work is included in this repository
 * The GrovePi+ libraries depend on Pi4J are maintained at http://pi4j.com/
 * and are retrieved by Maven.
 */
package com.kenfogel.temperature.bean;

import java.io.Serializable;

/**
 * This is the object that will be traveling through the web socket Based on the
 * PiyaraMicroDemo by Steve Millidge
 *
 * @author Ken Fogel
 * @version 0.1
 */
public class Temperature implements Serializable {

    private static final long serialVersionUID = 1L;
    private String symbol;
    private String description;
    private double temperature;

    public Temperature(String symbol, String description, double temperature) {
        this.symbol = symbol;
        this.description = description;
        this.temperature = temperature;
    }

    /**
     * Default constructor to satisfy serializable
     */
    public Temperature() {
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    /**
     * @return The string representation of the object in json format
     */
    @Override
    public String toString() {
        return "{" + "\"symbol\" :\"" + symbol + "\", \"description\" :\"" + description + "\", \"temperature\": " + temperature + '}';

    }

}
