/*

 DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.

 Copyright (c) 2015 C2B2 Consulting Limited. All rights reserved.

 The contents of this file are subject to the terms of the Common Development
 and Distribution License("CDDL") (collectively, the "License").  You
 may not use this file except in compliance with the License.  You can
 obtain a copy of the License at
 https://glassfish.dev.java.net/public/CDDL+GPL_1_1.html
 or packager/legal/LICENSE.txt.  See the License for the specific
 language governing permissions and limitations under the License.

 When distributing the software, include this License Header Notice in each
 file and include the License file at packager/legal/LICENSE.txt.
 */
/**
 * This demo is based on the PiyaraMicroDemo by Steve Millidge that can be found
 * at https://github.com/smillidge/PiyaraMicroDemo
 * The GrovePi+ libraries used in this project are based on the work of
 * Eduardo Moranchel that can be found at https://github.com/emoranchel/IoTDevices
 * and a modified version of Eduardo's work is included in this repository
 * The GrovePi+ libraries depend on Pi4J are maintained at http://pi4j.com/
 * and are retrieved by Maven.
 */
package com.kenfogel.temperature.ticker;

import com.kenfogel.temperature.bean.Temperature;
import fish.payara.micro.cdi.ClusteredCDIEventBus;
import fish.payara.micro.cdi.Outbound;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Startup;
import javax.ejb.Singleton;
import javax.ejb.Schedule;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import org.iot.raspberry.grovepi.GrovePi;
import org.iot.raspberry.grovepi.devices.GroveRgbLcd;
import org.iot.raspberry.grovepi.devices.GroveTemperatureAndHumiditySensor;
import org.iot.raspberry.grovepi.devices.GroveTemperatureAndHumidityValue;
import org.iot.raspberry.grovepi.pi4j.GrovePi4J;

/**
 * This is the class that collects the GrovePi+ data and sends it through the
 * web socket Based on the PiyaraMicroDemo by Steve Millidge
 *
 * @author martin
 * @author Ken Fogel
 * @version 0.1
 */
@Singleton
@Startup
public class TemperatureTicker {

    @Inject
    ClusteredCDIEventBus bus;

    @Inject
    @Outbound
    Event<Temperature> temperatureEvents;

    // The Grove objects come from Eduardo Moranchel libraries
    private GrovePi grovePi;
    private GroveTemperatureAndHumiditySensor dht;
    private GroveRgbLcd display;

    /**
     * This method initializes the web socket bus and instantiates the GrovePi
     * and Sensor objects. Many objects cannot interact with their container,
     * framework, or library until after the object is initialized that is after
     * the constructor finishes. Therefore we create methods that will be called
     * by the container, framework, or library after the constructor. These
     * methods are usually annotated with PostConstruct or with a required name
     * such as init.
     */
    @PostConstruct
    public void postConstruct() {
        bus.initialize();
        try {
            // Added to instantiate GrovePi4j and
            // GroveTemperatureAndHumiditySensor from Eduardo Moranchel's library
            grovePi = new GrovePi4J();
            dht = new GroveTemperatureAndHumiditySensor(grovePi, 4, GroveTemperatureAndHumiditySensor.Type.DHT11);
            display = grovePi.getLCD();
        } catch (IOException ex) {
            Logger.getLogger(TemperatureTicker.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    /**
     * This is the method that the EJB time will call based on the time period
     * defined by the Schedule annotation. In this code the method will be
     * called every 10 seconds.
     */
    @Schedule(hour = "*", minute = "*", second = "*/10", persistent = false)
    public void generateTemperature() {

        String symbol = "PAYARA";
        GroveTemperatureAndHumidityValue readings = null;

        // In the original code from Steve Millidge a random number to represent
        // a stock value was generated here. It now reads data from the DHT sensor
        try {
            readings = dht.get();
            display.setRGB(100, 100, 100);
            display.setText("Temp= " + readings.getTemperature());
        } catch (IOException ex) {
            Logger.getLogger(TemperatureTicker.class.getName()).log(Level.SEVERE, null, ex);
        }

        Temperature temperatureBean;
        temperatureBean = new Temperature(symbol, "", readings.getTemperature());
        Logger.getLogger(TemperatureTicker.class.getName()).log(Level.INFO, "generateTemperature:" + temperatureBean);
        temperatureEvents.fire(temperatureBean);

    }

}
